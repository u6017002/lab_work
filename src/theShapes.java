/**
 * Created by u6017002 on 1/03/17.
 */
public class theShapes {
    public static class Triangle implements TextDraw{

        @Override
        public void draw() {
            System.out.println("#");
            System.out.println("##");
            System.out.println("###");
            System.out.println("####");
        }
    }

    public static class Box implements TextDraw{

        @Override
        public void draw() {
            System.out.println("####");
            System.out.println("####");
            System.out.println("####");
        }
    }
}
