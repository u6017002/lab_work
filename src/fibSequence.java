import java.util.Scanner;

/**
 * Created by u6017002 on 1/03/17.
 */
public class fibSequence {

    public static int fib(int x){
        if (x == 2 || x == 1){
            return 1;
        }else{
            return fib(x-1) + fib(x - 2);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter a number to do the the fib sequence on: ");
        int x = scanner.nextInt();

        System.out.println(fib(x));
    }
}
